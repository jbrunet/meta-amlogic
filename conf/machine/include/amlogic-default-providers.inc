# Default providers for (lazy) amlogic machines

# linux kernel:
PREFERRED_PROVIDER_virtual/kernel ??= "linux-yocto"
PREFERRED_VERSION_linux-yocto ?= "6.6%"

# u-boot bootloader:
PREFERRED_PROVIDER_virtual/bootloader ??= "u-boot"
PREFERRED_PROVIDER_u-boot-fw-utils ??= "libubootenv"
MACHINE_EXTRA_RRECOMMENDS:append = ' ${@oe.utils.conditional("PREFERRED_PROVIDER_virtual/bootloader", "u-boot", "u-boot-fw-utils", "", d)}'

# Arm Trusted Firmware
PREFERRED_PROVIDER_virtual/trusted-firmware-a ??= "amlogic-prebuilt-atf"
